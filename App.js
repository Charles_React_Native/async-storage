import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import styled from "styled-components";
import Swiper from 'react-native-swiper';
import { onNewTaskAdded, todoService } from "./src/services/todo-service";
import Expo, { SQLite } from 'expo';
const db = SQLite.openDatabase('db.db');

const PageContainer = styled.View`
  display: flex;
  width: 100%;
  height: 100%;
  background-color: red;
  align-items: center;
  justify-content: center;
`;

const StyledView = styled.View`
  background-color: papayawhip;
  width: 100%;
`;

const StyledText = styled.Text`
  color: palevioletred;
  text-align:center;
  padding: 60px 0;
`;

export default class App extends React.Component {
  state = {
    tasks: []
  }

  componentDidMount(){
    db.transaction(tx => {
      tx.executeSql(
        'create table if not exists items (id integer primary key not null, done int, value text);'
      );
      tx.executeSql('insert into items (done, value) values (0, ?)', ["Teste"]);
      tx.executeSql('select * from items', [], (_, { rows }) =>
        console.log(rows["_array"])
      );
    });


    todoService.clearTasks();
    onNewTaskAdded.addListener('TASK_ADDED', (data) => {
      this.getTasks();
    });

    this.getTasks();
  }

  getTasks() {
    todoService.getTasks().
      then((tasks) => {
        this.setState({
          tasks
        })
      }).catch((err) => {
        console.log(err)
      });
  }

  getTaskLayout() {
    return this.state.tasks.map(t => {
      return (
        <Text>{ t.title } </Text>
      );
    })
  }

  saveTask() {
    todoService.saveTask({
      title: "asdfadfasdfasd"
    });
  }

  render() {
    return (
      <PageContainer>
        <StyledView>
          <StyledText>Hello World!</StyledText>
        </StyledView>
        <Swiper style={styles.wrapper} showsButtons={true}>
          <View style={styles.slide1}>
          { this.getTaskLayout() }
            <Text style={styles.text}>Hello Swiper</Text>
            <Button
                onPress={() => {
                  this.saveTask();
                }}
                title="Learn More"
                color="#841584"
                accessibilityLabel="Learn more about this purple button"
              />
          </View>
          <View style={styles.slide2}>
            <Text style={styles.text}>Beautiful</Text>
          </View>
          <View style={styles.slide3}>
            <Text style={styles.text}>And simple</Text>
          </View>
        </Swiper>
      </PageContainer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wrapper: {
  },
  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9DD6EB',
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#97CAE5',
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  }
});
