import {
    AsyncStorage
} from "react-native"
import EventEmitter from 'EventEmitter';

const TASK_STORAGE_NAME = "TASKS_STORAGE_TABLE";

export const onNewTaskAdded = new EventEmitter();
export const todoService = {
    saveTask: (task) => {
        return new Promise((resolve, reject) => {
            var tasks = [];
            AsyncStorage.getItem(TASK_STORAGE_NAME).then(data => {
                if(data){
                    tasks = JSON.parse(data);
                }
                tasks.push(task);
                AsyncStorage.setItem(`${TASK_STORAGE_NAME}`, JSON.stringify(tasks)).then(data => {
                    onNewTaskAdded.emit("TASK_ADDED", task);
                    resolve(task);
                }).catch(err => {
                    reject(err);
                });
            }).catch(err => {
                reject(err);
            });
        })
    },

    getTasks() {
        return new Promise((resolve, reject) => {
            AsyncStorage.getItem(TASK_STORAGE_NAME).then(data => {
                resolve(JSON.parse(data || []));
            }).catch(err => {
                reject(err);
            });
        })
    },

    deleteTask() {

    },

    clearTasks() {
        return new Promise((resolve, reject) => {
            AsyncStorage.removeItem(TASK_STORAGE_NAME).then(data => {
                resolve(JSON.parse(data));
            }).catch(err => {
                reject(err);
            });
        })
    }
}